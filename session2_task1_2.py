
# -*- coding: utf-8 -*-
""" Function to compute the area and the circumference of the circle 

    Input:
        User input the radius 

    Formula:
        Area = PI * radius * radius
        Circumference = 2 * PI * radius
    
    Function needs the radius to be provided and multiply with PI (3.14) 
    
    Return:
        Funtion return the area and circumference of the circle 
"""

def get_circle_area(radius):
    PI = 3.14
    area = PI * radius * radius
    return area

def get_circle_circum(radius):
    PI = 3.14
    circumference = 2 * PI * radius
    return circumference

radius = eval(input(' Please Enter the radius of a Circle: '))
area = get_circle_area(radius)
circumference = get_circle_circum(radius)
 
print(" Area Of a Circle = " + str(area))
print(" Circumference Of a Circle = %.2f" % float(circumference))