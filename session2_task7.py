
# -*- coding: utf-8 -*-
""" Function to judge whether the name of a person given is short or long

    Input:
        User input the name
    
    Return:
        Funtion judge whether the name of a person given is short or long
"""
def assess_name(name):
    length = len(name)
    if length <= 7:
        return "Short."
    elif length >= 10:
        return "Long."
    else:
        return "Perfect"

name = input("Enter the Name: ")
result = assess_name(name)

print("The given name " + name + " is " + result)
