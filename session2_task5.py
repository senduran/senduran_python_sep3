
# -*- coding: utf-8 -*-
""" Function to find and return the largest number out of 3 given numbers

     Input:
        User input 3 numbers


    Formula:
        def largest(num1, num2, num3):
    if (num1 > num2) and (num1 > num3):
        return num1
    elif (num2 > num1) and (num2 > num3):
        return num2
    else:
        return num3
    
    Function multiplies the P,N,R provided and divide by 100
    
    Return:
        Function return the largest of the 3 numbers
"""

def largest(num1, num2, num3):
    if (num1 > num2) and (num1 > num3):
        return num1
    elif (num2 > num1) and (num2 > num3):
        return num2
    else:
        return num3

num1 = int(input('Enter First number : '))
num2 = int(input('Enter Second number : '))
num3 = int(input('Enter Third number : '))

print("The largest of the 3 numbers is : ", largest(num1, num2, num3))
