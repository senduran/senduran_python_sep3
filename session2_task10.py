
def get_salary_amt(salary, employee_type="government", professional_tax=.25):
    if employee_type == "government":
        professional_tax = 0
    else:
        professional_tax = salary * .25 / 100
    salary_amt = salary - professional_tax
    return salary_amt


print(get_salary_amt(25000, "private"))
print(get_salary_amt(25000, "government"))