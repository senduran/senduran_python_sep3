
# -*- coding: utf-8 -*-
""" Function to compute whether the given character is a vowel or not

    Input:
        User input the Character

    Formula:
             if char == "a" or char == "e" or char == "i" or char == "o" or char == "u":
        return True
            else:
        return False
      
    Return:
        Function then compute the character and return boolean value
"""
     
def is_vowel(char):
    if char == "a" or char == "e" or char == "i" or char == "o" or char == "u":
        return True
    else:
        return False

char = input("Please enter the character to find whether it is a Vowel: ")

print(is_vowel(char))
