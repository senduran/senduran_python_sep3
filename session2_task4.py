
# -*- coding: utf-8 -*-
""" Function to compute the simple interest 

     Input:
        User input the principle amount (P), no. of years (N) and annual rate of interest (R)


    Formula:
        simp_int = (P*N*R)/100
        return simp_int    
    
    Function multiplies the P,N,R provided and divide by 100
    
    Return:
        Function return the simple interest 
"""
def get_int(P,N,R):
    simp_int = (P*N*R)/100
    return simp_int
    
P = eval(input("Enter the principle amount: "))
N = eval(input("Enter the time(years): "))
R = eval(input("Enter the rate: "))
    
simp_int = get_int(P,N,R)

print("The simple interest is:" ,get_int(P,N,R))
