
def get_selling_price(list_price):
    discount_percent = 10
    discount_amt = list_price * discount_percent / 100
    selling_price = list_price - discount_amt
    return selling_price

print(get_selling_price(100))
print(get_selling_price(200))
