# -*- coding: utf-8 -*-
""" Function to compute the given number is divisible by another given number

    Input:
        User input divident and divisor 

    Formula:
        remainder = divident % divisor

    Return:
        Funtion return whether the given number is divisible
"""
def div_finder(divident,divisor):
    remainder = divident % divisor
    if remainder != 0:
        return "Not divisible"
    else:
        return "Divisible"

a = int(input("What is the divident: "))
b = int(input("What is the divisor: "))
quo = a // b
v = div_finder(a,b)
print (v)
print ("And the quotient was", quo)